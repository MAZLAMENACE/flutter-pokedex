import 'package:flutter_tts/flutter_tts.dart';

class TtsService {
  final FlutterTts _flutterTts = FlutterTts();

  Future<FlutterTts> speak(String? text) async {
    await Future.delayed(const Duration(seconds: 1));
    await _flutterTts.setLanguage('fr-FR');
    await _flutterTts.setPitch(2);
    await _flutterTts.setSpeechRate(1.9);
    await _flutterTts.speak(text!);
    return _flutterTts;
  }

  void stopSpeak() async {
    await _flutterTts.stop();
  }

}