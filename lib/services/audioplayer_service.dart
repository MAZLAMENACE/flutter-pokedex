import 'package:audioplayers/audioplayers.dart';

class AudioPlayerService {
  final _player = AudioPlayer();

  void play(String songUrl) {
    _player.setSource(AssetSource(songUrl));
    _player.play(AssetSource(songUrl));
  }
}