import 'dart:convert';
import 'dart:math';
import 'package:http/http.dart' as http;
import 'package:pokedex/models/pokemon_model.dart';

class PokedexService {
  static const BASE_TYRADEX_API = 'https://tyradex.vercel.app/api/v1';
  static const BASE_POKE_API = 'https://pokeapi.co/api/v2/pokemon-species';

  Future<PokemonModel> getPokemon(num id) async {
    String url = '$BASE_TYRADEX_API/pokemon/$id';
    print(url);
    final response = await http.get(Uri.parse(url));
    if (response.statusCode == 200) {
      return PokemonModel.fromJson(jsonDecode(response.body));
    } else {
      throw Exception('Fail to load pokemon');
    }
  }

  Future<String?> getFrenchDescription(int id) async {
    final response = await http
        .get(Uri.parse('https://pokeapi.co/api/v2/pokemon-species/$id'));
    if (response.statusCode == 200) {
      final Map<String, dynamic> data = jsonDecode(response.body);
      final descriptions = data['flavor_text_entries'];

      for (var entry in descriptions) {
        final language = entry['language']['name'];
        final text = entry['flavor_text'];

        if (language == 'fr') {
          final inLineText = text.replaceAll("\n", " ");
          return inLineText;
        }
      }
    } else {
      throw Exception('Error getFrenchDescription');
    }
    return null;
  }
}
