import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:neon_widgets/neon_widgets.dart';
import 'package:pokedex/viewmodel/pokemon_viewmodel.dart';

class PokedexLightView extends StatelessWidget {
  PokedexLightView(this.pokemonViewModel, {super.key});

  PokemonViewModel? pokemonViewModel;

  @override
  Widget build(BuildContext context) {
    return pokemonViewModel!.isSpeaking
        ? const Positioned(
            top: 40,
            left: 60,
            child: Flicker(
                flickerTimeInMilliSeconds: 100,
                randomFlicker: false,
                child: NeonPoint(
                  pointColor: Colors.blueAccent,
                  spreadColor: Colors.blueAccent,
                  lightBlurRadius: 20,
                )))
        : const SizedBox.shrink();
  }
}
