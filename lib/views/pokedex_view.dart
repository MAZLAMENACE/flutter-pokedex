import 'package:flutter/material.dart';
import 'package:pokedex/viewmodel/pokemon_viewmodel.dart';
import 'package:pokedex/views/pokedex/podedex_light_view.dart';
import 'package:pokedex/views/pokemon/button_view.dart';
import 'package:pokedex/views/pokemon/description_view.dart';
import 'package:pokedex/views/pokemon/name_view.dart';
import 'package:pokedex/views/pokemon/sprite_view.dart';
import 'package:pokedex/views/pokemon/type_view.dart';
import 'package:provider/provider.dart';

class PokedexView extends StatefulWidget {
  const PokedexView({super.key});

  @override
  State<PokedexView> createState() => _PokedexViewState();
}

class _PokedexViewState extends State<PokedexView> {
  PokemonViewModel? pokemonViewModel;

  @override
  void initState() {
    pokemonViewModel = Provider.of<PokemonViewModel>(context, listen: false);

    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      //pokemonViewModel?.fetchPokemon(1);
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<PokemonViewModel>(
      builder: (context, viewModel, child) {
        return Stack(
          children: [
            Image.asset(
              'assets/images/fond.jpg',
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              fit: BoxFit.cover,
            ),
            Scaffold(
                backgroundColor: Colors.transparent,
                body: Center(
                  child: Stack(
                    children: [
                      Image.asset(
                        'assets/images/pokedex.png',
                        fit: BoxFit.contain,
                        height: 450,
                      ),
                      PokedexLightView(pokemonViewModel),
                      PokemonSpriteView(pokemonViewModel!),
                      PokemonNameView(pokemonViewModel!),
                      PokemonDescriptionView(pokemonViewModel!),
                      Positioned(
                        bottom: 95,
                        left: 42,
                        child: InkWell(
                          onTap: () => {
                            pokemonViewModel?.playSong('onoff.mp3'),
                            pokemonViewModel?.startPokedex(),
                          },
                          child: const CircleAvatar(
                              backgroundColor: Colors.transparent),
                        ),
                      ),
                      Positioned(
                          bottom: 50,
                          left: 390,
                          child: PokemonTypeView(pokemonViewModel!, 0)),
                      Positioned(
                          bottom: 50,
                          left: 500,
                          child: PokemonTypeView(pokemonViewModel!, 1)),
                      Positioned(
                        bottom: 67,
                        left: 254,
                        child: CircleAvatar(
                          backgroundColor: Colors.transparent,
                          child: IconButton(
                            iconSize: 20,
                            splashColor: Colors.transparent,
                            highlightColor: Colors.transparent,
                            color: Colors.white,
                            icon: const Icon(Icons.chevron_right),
                            onPressed: () {
                              pokemonViewModel?.playSong('plink.mp3');
                              pokemonViewModel?.getNextPokemon();
                            },
                          ),
                        ),
                      ),
                      Positioned(
                        bottom: 67,
                        left: 254,
                        child: CircleAvatar(
                          backgroundColor: Colors.transparent,
                          child: IconButton(
                            iconSize: 20,
                            splashColor: Colors.transparent,
                            highlightColor: Colors.transparent,
                            color: Colors.white,
                            icon: const Icon(Icons.chevron_right),
                            onPressed: () {
                              if (pokemonViewModel!.isOn) {
                                pokemonViewModel?.playSong('plink.mp3');
                                pokemonViewModel?.getNextPokemon();
                              }
                            },
                          ),
                        ),
                      ),
                      Positioned(
                        bottom: 67,
                        left: 203,
                        child: CircleAvatar(
                          backgroundColor: Colors.transparent,
                          child: IconButton(
                            iconSize: 20,
                            color: Colors.white,
                            icon: const Icon(Icons.chevron_left),
                            onPressed: () {
                              if (pokemonViewModel!.isOn) {
                                pokemonViewModel?.playSong('plink.mp3');
                                pokemonViewModel?.getPreviousPokemon();
                              }
                            },
                          ),
                        ),
                      ),
                      Positioned(
                        top: 237,
                        right: 213,
                        child: PokemonButtonView(pokemonViewModel!, "0"),
                      ),
                      Positioned(
                        top: 237,
                        right: 173,
                        child: PokemonButtonView(pokemonViewModel!, "1"),
                      ),
                      Positioned(
                        top: 237,
                        right: 133,
                        child: PokemonButtonView(pokemonViewModel!, "2"),
                      ),
                      Positioned(
                        top: 237,
                        right: 93,
                        child: PokemonButtonView(pokemonViewModel!, "3"),
                      ),
                      Positioned(
                        top: 237,
                        right: 53,
                        child: PokemonButtonView(pokemonViewModel!, "4"),
                      ),
                      Positioned(
                        top: 266,
                        right: 213,
                        child: PokemonButtonView(pokemonViewModel!, "5"),
                      ),
                      Positioned(
                        top: 266,
                        right: 173,
                        child: PokemonButtonView(pokemonViewModel!, "6"),
                      ),
                      Positioned(
                        top: 266,
                        right: 133,
                        child: PokemonButtonView(pokemonViewModel!, "7"),
                      ),
                      Positioned(
                        top: 266,
                        right: 93,
                        child: PokemonButtonView(pokemonViewModel!, "8"),
                      ),
                      Positioned(
                        top: 266,
                        right: 53,
                        child: PokemonButtonView(pokemonViewModel!, "9"),
                      ),
                      Positioned(
                          top: 322,
                          right: 213,
                          child: InkWell(
                            onTap: () => {
                              if (pokemonViewModel!.isOn)
                                {pokemonViewModel?.resetPokemonId()}
                            },
                            child: const CircleAvatar(
                              backgroundColor: Colors.transparent,
                              radius: 13,
                            ),
                          )),
                      Positioned(
                          top: 322,
                          right: 173,
                          child: InkWell(
                            onTap: () => {
                              if (pokemonViewModel!.isOn)
                                {
                                  pokemonViewModel?.playSong('levelup.mp3'),
                                  pokemonViewModel?.searchPokemonById(),
                                }
                            },
                            child: const CircleAvatar(
                              backgroundColor: Colors.transparent,
                              radius: 13,
                            ),
                          )),
                    ],
                  ),
                ))
          ],
        );
      },
    );
  }
}
