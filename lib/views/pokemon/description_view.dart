import 'package:flutter/material.dart';
import 'package:pokedex/viewmodel/pokemon_viewmodel.dart';

class PokemonDescriptionView extends StatelessWidget {
  PokemonDescriptionView(this.pokemonViewModel, {super.key});

  PokemonViewModel pokemonViewModel;

  @override
  Widget build(BuildContext context) {
    return Positioned(
        top: 148,
        right: 48,
        child: SizedBox(
            width: 200,
            height: 75,
            child: Padding(
                padding: const EdgeInsets.all(10),
                child: SingleChildScrollView(
                  child: Text(
                    pokemonViewModel.currentPokemon?.description ?? '',
                    style: const TextStyle(
                      color: Colors.white,
                      fontSize: 8,
                      fontFamily: 'PressStart2P',
                    ),
                  ),
                ))));
  }
}
