import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pokedex/viewmodel/pokemon_viewmodel.dart';
import 'package:transparent_image/transparent_image.dart';

class PokemonSpriteView extends StatelessWidget {
  PokemonSpriteView(this.pokemonViewModel, {super.key});

  PokemonViewModel pokemonViewModel;

  @override
  Widget build(BuildContext context) {
    return Positioned(
      bottom: 183,
      left: 68,
      child: Container(
          width: 195,
          height: 130,
          color: !pokemonViewModel.isOn
                  ? Colors.black : 
                  Colors.white60,
          child: Center(
              child: pokemonViewModel.currentPokemon?.sprites?.regular == null
                  ? null
                  :
                  // Image.asset('assets/images/loader.gif', width: 200);
                  Image.network(
                      pokemonViewModel.currentPokemon!.sprites!.regular!,
                      fit: BoxFit.fill,
                    ))),
    );
  }
// Image.network(pokemonViewModel.currentPokemon!.sprites!.regular!);
}
