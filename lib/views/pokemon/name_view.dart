import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../viewmodel/pokemon_viewmodel.dart';

class PokemonNameView extends StatelessWidget {
  PokemonNameView(this.pokemonViewModel, {super.key});
  PokemonViewModel pokemonViewModel;

  @override
  Widget build(BuildContext context) {
    return  Positioned(
        bottom: 35,
        left: 85,
        child:
        SizedBox(
          width: 100,
          height: 45,
          child: Center(
            child: Text(
              pokemonViewModel.currentPokemon?.name?.fr ?? '',
              overflow: TextOverflow.ellipsis,
              style: const TextStyle(
                fontSize: 9.0,
                fontFamily: 'PressStart2P',
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
        ));
  }
}
