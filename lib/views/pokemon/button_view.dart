import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../viewmodel/pokemon_viewmodel.dart';

class PokemonButtonView extends StatelessWidget {
  PokemonButtonView(this.pokemonViewModel, this.number, {super.key});

  PokemonViewModel pokemonViewModel;
  String number;

  @override
  Widget build(BuildContext context) {
    return InkWell(
        onTap: () => { 
          if(pokemonViewModel.isOn){
            pokemonViewModel.playSong('plink.mp3'),
            pokemonViewModel.pokemonId = number,
          }
        },
        child: CircleAvatar(
          backgroundColor: Colors.transparent,
          radius: 13,
          child: Text(
            number,
            style: const TextStyle(
              color: Colors.white,
              fontSize: 16.0,
              fontWeight: FontWeight.bold,
            ),
          ),
        ));
  }
}
