import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../viewmodel/pokemon_viewmodel.dart';

class PokemonTypeView extends StatelessWidget {
  PokemonTypeView(this.pokemonViewModel, this.index, {super.key});

  PokemonViewModel pokemonViewModel;
  int index;

  @override
  Widget build(BuildContext context) {
    String? type = pokemonViewModel.isInList(index);

    return Text(
          type ?? '',
          overflow: TextOverflow.ellipsis,
          style: const TextStyle(
            color: Colors.white,
            fontSize: 9.0,
            fontFamily: 'PressStart2P',
            fontWeight: FontWeight.bold,
          ),
        );
  }
}
