class PokemonModel {
  int? pokedexId;
  int? generation;
  String? category;
  Name? name;
  Sprites? sprites;
  List<Types>? types;
  String? height;
  String? weight;
  String? description;

  PokemonModel(
      {this.pokedexId,
        this.generation,
        this.category,
        this.name,
        this.sprites,
        this.types,
        this.height,
        this.weight,
        this.description});

  PokemonModel.fromJson(Map<String, dynamic> json) {
    pokedexId = json['pokedex_id'];
    generation = json['generation'];
    category = json['category'];
    name = json['name'] != null ? new Name.fromJson(json['name']) : null;
    sprites =
    json['sprites'] != null ? new Sprites.fromJson(json['sprites']) : null;
    if (json['types'] != null) {
      types = <Types>[];
      json['types'].forEach((v) {
        types!.add(new Types.fromJson(v));
      });
    }
    height = json['height'];
    weight = json['weight'];

    var typesList = types?.length;
    String text = "${name?.fr}, est un $category de type ";

    if(typesList == 1){
      text+="${types![0].name}";
    }else{
      for(var i = 0; i<typesList!; i++){
        text+="${types![i].name} et ";
      }
      text = text.substring( 0, text.length-3);
    }

    description = text;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['pokedex_id'] = this.pokedexId;
    data['generation'] = this.generation;
    data['category'] = this.category;
    if (this.name != null) {
      data['name'] = this.name!.toJson();
    }
    if (this.sprites != null) {
      data['sprites'] = this.sprites!.toJson();
    }
    if (this.types != null) {
      data['types'] = this.types!.map((v) => v.toJson()).toList();
    }
    data['height'] = this.height;
    data['weight'] = this.weight;
    return data;
  }
}

class Name {
  String? fr;
  String? en;
  String? jp;

  Name({this.fr, this.en, this.jp});

  Name.fromJson(Map<String, dynamic> json) {
    fr = json['fr'];
    en = json['en'];
    jp = json['jp'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['fr'] = this.fr;
    data['en'] = this.en;
    data['jp'] = this.jp;
    return data;
  }
}

class Sprites {
  String? regular;
  String? shiny;

  Sprites({this.regular, this.shiny});

  Sprites.fromJson(Map<String, dynamic> json) {
    regular = json['regular'];
    shiny = json['shiny'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['regular'] = this.regular;
    data['shiny'] = this.shiny;
    return data;
  }
}

class Types {
  String? name;
  String? image;

  Types({this.name, this.image});

  Types.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    image = json['image'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['image'] = this.image;
    return data;
  }
}
