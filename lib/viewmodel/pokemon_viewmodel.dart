import 'dart:convert';
import 'dart:developer';

import 'package:flutter/cupertino.dart';
import 'package:pokedex/models/pokemon_model.dart';
import 'package:pokedex/services/audioplayer_service.dart';
import 'package:pokedex/services/pokedex_service.dart';
import 'package:flutter_tts/flutter_tts.dart';
import 'package:pokedex/services/tts_service.dart';

class PokemonViewModel extends ChangeNotifier {
  PokemonModel? _currentPokemon;
  bool _isSpeaking = false;
  String _pokemonId = "";
  bool _isOn = false;

  final TtsService _ttsService = TtsService();
  final PokedexService _pokedexService = PokedexService();
  final AudioPlayerService _audioPlayerService = AudioPlayerService();

  bool get isSpeaking => _isSpeaking;
  String get pokemonId => _pokemonId;
  bool get isOn => _isOn;

  set isSpeaking(bool value) {
    _isSpeaking = value;
    notifyListeners();
  }

  set pokemonId(String number) {
    _pokemonId= "$_pokemonId$number";
  }

  set isOn(bool on){
    _isOn = on;
  }

  PokemonModel? get currentPokemon => _currentPokemon;

  set currentPokemon(PokemonModel? value) {
    _currentPokemon = value;
    notifyListeners();
  }

  Future<String?> fetchPokemonDescription(int id) async {
    return await _pokedexService.getFrenchDescription(id);
  }

  void fetchPokemon(int id) async {
    try {

      PokemonModel fetchedPokemon = await _pokedexService.getPokemon(id);
      String? description = await _pokedexService.getFrenchDescription(id);
      if (description != null) {
        fetchedPokemon.description =
            '${fetchedPokemon.description}. ${description.trim()}';
      }
      currentPokemon = fetchedPokemon;
      isSpeaking = true;
      _speakText(currentPokemon?.description);
    } catch (err) {
      debugPrint('Error in fetchPokemon :${err.toString()}');
    }
  }

  void getNextPokemon() {
    if(isSpeaking) {
      _ttsService.stopSpeak();
      isSpeaking = false;
    }
    int nextId = 1;
    if (currentPokemon != null) {
      nextId = currentPokemon!.pokedexId! + 1;
    }
    fetchPokemon(nextId);
  }

  void getPreviousPokemon() {
    if(isSpeaking) {
      _ttsService.stopSpeak();
      isSpeaking = false;
    }
    int previousId = 1;
    if (currentPokemon != null) {
      previousId = currentPokemon!.pokedexId! - 1;
    }

    if (previousId >= 1) {
      fetchPokemon(previousId);
    }
  }

  void playSong(String songUrl) {
    _audioPlayerService.play('sounds/$songUrl');
  }

  Future<void> _speakText(String? text) async {
    final ttsInstance = await _ttsService.speak(text);
    ttsInstance.setCompletionHandler(() {
      isSpeaking = false;
    });
  }

  String isInList(int index) {
    String? type;
    if (index == currentPokemon?.types!.length) {
      type = "";
    } else {
      type = currentPokemon?.types![index].name;
    }
    return type ?? '';
  }

  void resetPokemonId(){
    _pokemonId = "";
  }

  void searchPokemonById(){

    if(_pokemonId.isNotEmpty){
      int pokemonId = int.parse(_pokemonId);

      if(pokemonId>0 && pokemonId<=151){
        fetchPokemon(pokemonId);
        resetPokemonId();
      }else{
        PokemonModel mascime = PokemonModel.fromJson(jsonDecode('{"pokedex_id": 0, "generation": 1, "category": "Pok\u00e9mon chauve","name": { "fr": "Mascime"}, "sprites": {"regular": "https://i.imgur.com/Me8Y6Yo.jpg"},"types": [{"name": "Roche","image": "https://raw.githubusercontent.com/Yarkis01/TyraDex/images/types/plante.png"},{"name": "Dragon","image": "https://raw.githubusercontent.com/Yarkis01/TyraDex/images/types/poison.png"}],"height": "0,7 m","weight": "6,9 kg", "description" : "" }'));
        mascime.description = "${mascime.description} Généralement tapis au fond de ça taverne, il en ressort quelque fois pour emmagasiner de l'énergie grâce à son crâne.";
        currentPokemon = mascime;
        isSpeaking = true;
        _speakText(currentPokemon?.description);
        resetPokemonId();
      }
    }
  }

  void startPokedex(){
    if(!isOn){
      notifyListeners();
    }else{
      currentPokemon = null;
      resetPokemonId();
      _ttsService.stopSpeak();
      isSpeaking = false;
    }

    isOn = !isOn;
  }
}
